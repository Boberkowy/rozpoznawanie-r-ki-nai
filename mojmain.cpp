#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <unistd.h>

using namespace std;
using namespace cv;
String haarcascade = "renka.xml";

void HandDetection(Mat frame);


int main(){
	
	VideoCapture cap(0);
	CascadeClassifier hand_cascade;
	Mat frame, frame_gray;
	vector<Rect> hand;
	
	if(!hand_cascade.load( haarcascade )){
		cout << " Nie mogę wczytać pliku .xml " << endl ;
		return -1;
	}
	 if(!cap.isOpened()) {
		cout << " Nie mogę odpalić kamerki " << endl; 
        return -1;	
	}
	while(true ){
		cap >> frame;
	
	    cvtColor(frame, frame_gray, CV_BGR2GRAY);
		equalizeHist(frame_gray, frame_gray);
		imshow ("equalizeHist", frame_gray);
	
	 hand_cascade.detectMultiScale(frame, hand, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(100,100));
	 
	  for( size_t i = 0; i < hand.size(); i++ ){
		  Point center ( hand[i].x + hand[i].width* 0.5, hand[i].y + hand[i].height*0.5);
		  ellipse (frame, center,Size(hand[i].width*0.5, hand[i].height*0.5), 0,0, 360, Scalar( 255,255,255), 4,8,0);
		int x = hand[i].x ;
		int y = hand[i].y ;
	
		cout << " x = "  << x << " y = "  << y <<endl;
		
		
		if(x > 400 && y <= 100)
		system("firefox"); // góraprawy
		 
		if(x < 100 && y <= 100){
		system ("libreoffice --writer "); // góra lewy
		}
		
		if(x < 100 && y > 350)
	    system("eog -f da.jpg"); // dół lewy
	    
	    if(x > 500 && y > 350)
		system("eog -f medytacja.jpg"); // dół lewy
	}
		

	   circle(frame, Point(600,30) , 20, Scalar(0,0,0), 2, 8, 0 ); // góra prawe
	   circle(frame, Point(30,30) , 20, Scalar(0,0,0), 2, 8, 0 ); // góra lewe
	   circle(frame, Point(30, 450) , 20, Scalar(0,0,0), 2, 8, 0 ); //dół lewy
	   circle(frame, Point(600,450) , 20, Scalar(0,0,0), 2, 8, 0 ); //dół prawy

	  imshow("Reka", frame);
	 
        if( (waitKey(30) == 27 ) ){
			cout << " No elo " << endl;
			 break;
			  }
	}
	return 0;
}
